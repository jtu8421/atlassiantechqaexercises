"""
    Test environment:
        Python 2.7
        Python package: selenium
"""

__author__ = "Jun Tu"
__email__ = "jarvinia.tu@gmail.com"

import unittest

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from page import ProjectDefaultPage

USERNAME = "jarvinia.tu@gmail.com"
PASSWORD = "jtuJIRA2015"
 
class TestJiraCreateIssues(unittest.TestCase):
 
    def setUp(self):
        self.driver=webdriver.Firefox()
        self.testpage = ProjectDefaultPage(self.driver)
        self.testpage.open()
        
    def testCreateIssueLinkWithoutLogin(self):       
        with self.assertRaises(NoSuchElementException):
            self.testpage.locate_create_button()
        
    def testNewIssueNotFillInSummaryForm(self):
        self.testpage.load_login_page(USERNAME, PASSWORD)
        self.testpage.load_create_form()
        self.testpage.submit_create_issue_form()
        
        self.assertTrue(self.testpage.is_error())
        
    def testCreateIssueOK(self):
        self.testpage.load_login_page(USERNAME, PASSWORD)
        self.testpage.load_create_form()
        self.testpage.fill_in_summary_field("Test create issue - Jun Tu")
        self.testpage.submit_create_issue_form()
        
        self.assertFalse(self.testpage.is_error())
        self.testpage.wait(10)
        self.assertIn("A Test Project - Atlassian JIRA", self.testpage.get_title()) 
        
    def tearDown(self):
        self.driver.close()
        
if __name__=="__main__":
    unittest.main()