== Test environment
* Python 2.7
* Firefox
* Python bindings for Selenium
* Windows

== Installation
Please find installation guide from http://selenium-python.readthedocs.org/en/latest/installation.html.

== Test 
1. copy those 2 scripts "page.py" and "JiraCreateIssues.py" to your workspace
2. Start a command prompt using the cmd.exe program and run
    set PATH=%Python2.7_install_path%;%PATH%
3. Change directory to your workspace using the command
    cd %Your_workspace%
4. Start to run test by typing 
    python JiraCreateIssues.py
