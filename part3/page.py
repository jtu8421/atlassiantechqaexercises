
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class BasePage(object):
    def __init__(self, webdriver):
        self.webdriver = webdriver
        

class ProjectDefaultPage(BasePage):
    CREATE_LOC = (By.ID, 'create_link')
    LOGIN_LOC = (By.LINK_TEXT, 'Log In')
    USERNAME_LOC = (By.ID, 'username')
    PASSWORD_LOC = (By.ID, 'password')
    LOGIN_SUBMIT_BUTTON_LOC = (By.ID,'login-submit')
    CREATE_LINK_LOC = (By.ID, 'create_link')
    CREATE_SUBMIT_LOC = (By.ID, 'create-issue-submit')
    ERR_CLASS_LOC = (By.CLASS_NAME, "error")
    
    def open(self):
        self.webdriver.get("https://jira.atlassian.com/browse/TST")
        
    def locate_create_button(self):
        self.webdriver.find_element(*self.CREATE_LOC)
        
    def get_title(self):
        return self.webdriver.title
        
    def load_login_page(self, username, password):
        self.webdriver.find_element(*self.LOGIN_LOC).click()
        
        self.webdriver.find_element(*self.USERNAME_LOC).send_keys(username)
        self.webdriver.find_element(*self.PASSWORD_LOC).send_keys(password)
        self.webdriver.find_element(*self.LOGIN_SUBMIT_BUTTON_LOC).submit()
        
        self.webdriver.implicitly_wait(3)
    
    def load_create_form(self):
        self.webdriver.find_element(*self.CREATE_LINK_LOC).click()
        
    def submit_create_issue_form(self):
        self.webdriver.find_element(*self.CREATE_SUBMIT_LOC).click()
        
    def is_error(self):
        try:
            self.webdriver.find_element(*self.ERR_CLASS_LOC)
        except NoSuchElementException: 
            return False
        
        return True
    
    def fill_in_summary_field(self, summary_txt):
        summary_element = self.webdriver.find_element_by_xpath("//input[@id='summary']")
        summary_element.clear()
        summary_element.send_keys(summary_txt)
        
    def wait(self, seconds):
        self.webdriver.implicitly_wait(seconds)
        